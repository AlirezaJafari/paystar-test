function UnixToISODate(unix, time = false) {
    let timezone_offset = new Date().getTimezoneOffset() * 60000;
    if (unix == "") unix = 0;
    if (time)
        return (((new Date(parseInt(unix) - timezone_offset)).toISOString()).substr(0, 19)).replace("T", " ");
    return (((new Date(parseInt(unix))).toISOString()).substr(0, 10));
}
