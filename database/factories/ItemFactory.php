<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\Modules\Order\Entities\Item::class, function (Faker $faker) {
    return [
        'product_name' => 'product',
        'quantity' => $faker->numberBetween(1, 10),
        'price' => 10000
    ];
});
