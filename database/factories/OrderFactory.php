<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Order\Entities\Order;
use Modules\User\Entities\User;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'total_price' => $faker->randomFloat(2, 10, 100),
        'status' => $faker->randomElement(['pending', 'processing', 'completed']),
        'address' => $faker->address,
        'description' => $faker->sentence,
        'bank_number' => $faker->creditCardNumber,
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
