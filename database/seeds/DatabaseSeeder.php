<?php

use Illuminate\Database\Seeder;
use Modules\Order\Entities\Item;
use Modules\Order\Entities\Order;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Item::class, 10)->create();
        factory(Order::class, 10)->create();
        // $this->call(UserSeeder::class);
    }
}
