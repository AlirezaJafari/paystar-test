<?php

namespace seeds;

use Illuminate\Database\Seeder;
use Modules\Order\Entities\Item;

class OrderDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Item::class, 10)->create(); // Use the factory helper function
        dd(451);
        // $this->call("OthersTableSeeder");
    }
}
