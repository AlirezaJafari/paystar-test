<?php

namespace Modules\Auth\Http\Controllers\Api\Auth;

use Modules\Order\Http\Api\Controller;
use App\Http\Requests\RegisterUserRequest;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;

class RegisterController extends Controller
{
    public function register(RegisterUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'bank_number' => $request->bank_number, // Save bank_number
        ]);

        $token = $user->createToken('API Token')->plainTextToken;

        return response()->json(['token' => $token], 201);
    }
}
