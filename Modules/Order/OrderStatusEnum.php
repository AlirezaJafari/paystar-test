
<?php

//namespace App\Enums;
//
//use Spatie\Enum\Enum;
//
///**
// * @method static self todo()
// * @method static self in_progress()
// * @method static self completed()
// * @method static self nullable()  // Define the nullable status
// */
//class TodoStatusEnum extends Enum
//{
//    protected static function values(): array
//    {
//        return [
//            'todo' => 'To Do',
//            'in_progress' => 'In Progress',
//            'completed' => 'Completed',
//        ];
//    }
//}
