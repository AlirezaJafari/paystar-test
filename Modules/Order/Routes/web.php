<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Order\Http\Controllers\OrderController;


Route::prefix('order')->group(function() {
    Route::get('/', 'OrderController@index');
});



Route::middleware('auth')->resource('order', OrderController::class);


// Define the route without CSRF protection
Route::match(['post', 'get'], '/callback', function (Request $request) {
    // Handle both POST and GET requests here
    if ($request->isMethod('post')) {
        // Handle POST request
        dd($request->all());
    } else {
        // Handle GET request
        dd($request->all());
    }
})->withoutMiddleware(['web', 'csrf']);

