<?php

namespace Modules\Order\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Repositories\OrderRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrderRepository::class, OrderRepositoryEloquent::class);
    }
}
