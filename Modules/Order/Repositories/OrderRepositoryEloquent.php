<?php

namespace Modules\Order\Repositories;

use Modules\Order\Entities\Order;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OrderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }
    public function createOrderWithItems(array $data, $userId = null)
    {
        // Extract order data from the request
        $orderData = $data['order'];
        $totalPrice = $data['total_price'];

        // Create the order
        $order = Order::create([
            'total_price' => $totalPrice,
            'status' => 'done',
            'description' => $orderData['description'],
            'address' => $orderData['address'],
            'bank_number' => $orderData['bank_number'],
            'user_id' => $userId,
        ]);

        // Attach items to the order dynamically based on the items_ids array
        foreach ($data['items_ids'] as $itemIds) {
            $order->items()->attach($itemIds);
        }

        return $order;
    }




    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
