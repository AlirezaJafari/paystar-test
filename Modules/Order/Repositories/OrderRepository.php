<?php

namespace Modules\Order\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderRepository extends RepositoryInterface
{
     public function createOrderWithItems(array $data, $userId = null);
}
