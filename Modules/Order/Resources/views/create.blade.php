@extends('layout.admin')
@section('title', 'سفارشات ')

@section('body')

    <div class="content-wrapper" style="min-height: 845px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="row-cols-sm-6">
                        <h1>@yield('title')</h1>
                    </div>
                </div><!-- /.container-fluid -->
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    @if ($errors->any())
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <strong>خطا!</strong> لطفاً خطاهای زیر را اصلاح کنید:<br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            محصولات
                        </h3>

                        <div class="card-tools">
                            <ul class="pagination pagination-sm">
                                <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                                <li class="page-item"><a href="#" class="page-link">1</a></li>
                                <li class="page-item"><a href="#" class="page-link">2</a></li>
                                <li class="page-item"><a href="#" class="page-link">3</a></li>
                                <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Replace the following card sections with your desired content -->
                        @foreach($items as $item)
                            <div class="col-md-4">
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <h3 class="profile-username text-center">{{ $item['product_name'] }}</h3>
                                        <p class="text-muted text-center">تعداد: {{ $item['quantity'] }}</p>
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>قیمت</b> <span class="float-right">{{ $item['price'] }}</span>
                                            </li>
                                            <!-- Add more details here if needed -->
                                        </ul>
                                        <button type="button" class="btn btn-outline-info btn-block" data-toggle="modal" data-target="#exampleModal{{ $loop->index }}"><b>انتخاب</b></button>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{ $loop->index }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">اطلاعات پرداخت</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Add your form fields for address and bank number here -->
                                                <form action="{{ route('order.store') }}" method="POST">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="address">آدرس:</label>
                                                        <textarea class="form-control" id="address" name="order[address]" rows="3" placeholder="آدرس را وارد کنید"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="bank_number">شماره کارت:</label>
                                                        <input type="text" class="form-control" id="bank_number" name="order[bank_number]" placeholder="شماره کارت را وارد کنید">
                                                    </div>
                                                    <!-- Hidden input fields for order data -->
                                                    <input type="hidden" name="order[description]" value="Order description">
                                                    <input type="hidden" name="order[done_at]" value="{{ now()->toDateString() }}">
                                                    <input type="hidden" name="items_ids[{{ $loop->index }}][]" value="{{ $item['id'] }}">
                                                    <input type="hidden" name="total_price" value="{{ $item['price'] }}">
{{--                                                    <input type="hidden" name="order_id" value="{{ $order->id }}">--}}
                                                    <button type="submit" class="btn btn-success"><i class="fa fa-credit-card"></i> ثبت سفارش</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('layout.js')
    @yield('js')
    </body>
    </html>
