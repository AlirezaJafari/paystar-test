@extends('layout.admin')
@section('title', 'صورتحساب')

@section('body')

    <div class="content-wrapper" style="min-height: 845px;">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <strong>خطا!</strong> لطفاً خطاهای زیر را اصلاح کنید:<br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>صورتحساب</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><a href="#">خانه</a></li>
                            <li class="breadcrumb-item active">صورتحساب</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    <strong>از:</strong>
                                    <address>
                                        <!-- Your address details here -->
                                    </address>
                                </div>
                                <div class="col-sm-4 invoice-col">
                                    <strong>به:</strong>
                                    <address>
                                        <!-- Recipient's address details here -->
                                    </address>
                                </div>
                            </div><!-- /.row -->

                            <!-- Table row -->
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>قیمت کل</th>
                                            <th>ادرس</th>
                                            <th>وضعیت</th>
{{--                                            <th>توضیحات</th>--}}

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>{{ $order->total_price ?? '' }}</td>

                                            <td>{{ $order->address ?? '' }}</td>

                                            <td>{{ $order->status ?? '' }}</td>
{{--                                            <td>{{ $order->desciption ?? '' }}</td>--}}
                                            <!-- Add more rows as needed -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- /.row -->

                            <!-- Invoice total row -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- /.row -->

                            <!-- Print and payment buttons -->
                            <div class="row no-print">
                                <div class="col-12">
                                    <button onclick="window.print()" target="_blank" class="btn btn-default">
                                        <i class="fa fa-print"></i> پرینت
                                    </button>
                                    <form id="paymentForm" action="{{ route('payment') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="total_price" value="{{ $order->total_price }}">
                                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                                        <button type="submit" class="btn btn-success float-left">
                                            <i class="fa fa-credit-card"></i> پرداخت صورتحساب
                                        </button>
                                    </form>
                                    <button type="button" class="btn btn-primary float-left ml-2">
                                        <i class="fa fa-download"></i> تولید PDF
                                    </button>
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection

@section('js')
    <!-- Custom JS code goes here -->
@endsection
