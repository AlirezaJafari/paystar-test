<?php

namespace Modules\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'order.address' => 'required|string|max:255',
            'order.bank_number' => 'required|string|max:255',
            'order.description' => 'required|string|max:255',
            'order.done_at' => 'required|date',
            'items_ids.*' => 'required|array',
            'items_ids.*.*' => 'required|exists:items,id', // Assuming 'items' is the table name
            'total_price' => 'required|numeric|min:0',
        ];
    }
}
