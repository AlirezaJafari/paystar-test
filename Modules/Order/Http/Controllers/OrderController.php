<?php

// app/Http/Controllers/OrderController.php

namespace Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Order\Entities\Item;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Requests\OrderRequest;
use Modules\Order\Repositories\OrderRepository;

class OrderController
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function create()
    {

         $items =Item::all();
        return view('order::create',compact('items'));
    }

    public function store(OrderRequest $request)
    {

        $user = auth()->user();
       $order =  $this->orderRepository->createOrderWithItems($request->all(), $user->id);

        return redirect()->route("order.show", ['order' => $order->id])->with('success', 'The order created successfully');

    }

    public function Show(string $id)
    {
    $order = Order::find($id);

        return view('order::show',compact('order'));
    }
}
