<?php

namespace Modules\Order\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Resources\OrderResource;
use Modules\Order\Repositories\OrderRepository;

class OrderController
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    public function index()
    {
        $orders = Order::all();
        return OrderResource::collection($orders);
    }

    public function store(Request $request)
    {
        // Get the authenticated user
        $user = auth()->user();

        // Check if a user is authenticated
        if ($user) {
            // Use the authenticated user's ID
            $userId = $user->id;
        } else {
            // If no user is authenticated, set the user ID to null or handle the case as needed
            $userId = null;
        }

        // Call the repository method to create the order
        $order = $this->orderRepository->createOrderWithItems($request->all(), $userId);

        // Return a JSON response indicating success
        return new OrderResource($order);
    }

    public function show($id)
    {
        $order = Order::find($id);
        return new OrderResource($order);
    }
}
