<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Payment\Entities\PaymentTransaction;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'address','bank_number', 'total_price', 'status', 'user_id'];


    /**
     * Get the items for the order.
     */
    public function items()
    {
        return $this->belongsToMany(Item::class)->withPivot('quantity', 'price')->withTimestamps();
    }

    public function paymentTransactions()
    {
        return $this->hasMany(PaymentTransaction::class);
    }

    // Your existing code...

    /**
     * Mark the order as paid.
     *
     * @return bool
     */
    public function markAsPaid()
    {
        $this->status = "Settled";
        return $this->save();
    }
}


