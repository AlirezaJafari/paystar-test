<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Order\Entities\Order; // Assuming there's an Order model

class Item extends Model
{
    protected $fillable = ['product_name', 'quantity', 'price'];

    /**
     * Get the orders that belong to this item.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot('quantity', 'price')->withTimestamps();
    }

    // Add any other relationships as needed
}
