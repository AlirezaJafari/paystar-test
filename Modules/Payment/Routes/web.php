<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Payment\Http\Controllers\PaymentController;

Route::prefix('payment')->group(function() {
    Route::get('/', 'PaymentController@index');
});


Route::post('payment', [PaymentController::class, 'createTransaction'])->name('payment');
Route::any('verify', [PaymentController::class, 'verifyTransaction'])->name('payment-verify')->withoutMiddleware(['web', 'csrf']);
