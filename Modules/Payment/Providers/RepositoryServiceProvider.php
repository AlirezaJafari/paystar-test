<?php

namespace Modules\Payment\Providers;

use Illuminate\Support\ServiceProvider;;
use Modules\Payment\Repositories\PaymentRepository;
use Modules\Payment\Repositories\PaymentRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentRepository::class, PaymentRepositoryEloquent::class);
    }
}
