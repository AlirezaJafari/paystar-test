<?php

namespace Modules\Payment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'total_price' => 'required|numeric|min:0',
            'order_id' => 'required|string|max:255',
            // Add more validation rules as needed
        ];
    }
}
