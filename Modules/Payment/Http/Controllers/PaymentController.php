<?php

namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Modules\Payment\Entities\PaymentTransaction;
use Modules\Payment\Http\Requests\PaymentRequest;
use Modules\Payment\Services\CreateSignService;
use Modules\Payment\Services\CreateTransaction;
use Modules\Payment\Services\VerifyTransaction;

//    protected $paymentService;

//    public function __construct(PaymentService $paymentService)
//    {
//        $this->paymentService = $paymentService;
//    }
//

class PaymentController
{
    protected $gatewayId;
    protected $code;
    protected $createSignService;
    protected $createTransaction;
    protected $verifyTransaction;

    public function __construct(CreateSignService $createSignService,CreateTransaction $createTransaction,VerifyTransaction $verifyTransaction)
    {
        $this->createSignService = $createSignService;
        $this->createTransaction = $createTransaction;
        $this->verifyTransaction = $verifyTransaction;

    }

    public function createTransaction(PaymentRequest $request)
    {

     $paymentTransaction =  $this->createTransaction->createPaymentTransaction($request->all());
        return view('tet',compact('paymentTransaction'));

    }


    public function verifyTransaction(Request $request)
    {
       $payment = $this->verifyTransaction->verify($request->all());
        return redirect()->route("order.show", ['order' => $payment->order_id])->with('success', 'The Service verified successfully');
}


}
