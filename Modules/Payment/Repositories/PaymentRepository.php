<?php

namespace Modules\Payment\Repositories;
use Modules\Payment\Entities\PaymentTransaction;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface PaymentRepository extends RepositoryInterface
{
    public function createPaymentTransaction($data);

    public function updateTransactionFields(int $transactionId, array $fields): ?PaymentTransaction;

    public function findByRefNum($refNum);
}
}
