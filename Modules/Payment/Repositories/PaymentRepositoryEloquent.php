<?php

namespace Modules\Payment\Repositories;

use Modules\Order\Entities\Order;
use Modules\Payment\Entities\PaymentTransaction;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OrderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PaymentRepositoryEloquent extends BaseRepository implements PaymentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentTransaction::class;
    }
    public function createPaymentTransaction($data)
    {
        return PaymentTransaction::create($data);

    }

    public function updateTransactionFields(int $transactionId, array $fields): ?PaymentTransaction
    {
        $transaction = PaymentTransaction::find($transactionId);
        if ($transaction) {
            $transaction->update($fields);
        }
        return $transaction;
    }
    public function findByRefNum($refNum)
    {
        return $this->model->where('ref_num', $refNum)->first();
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
