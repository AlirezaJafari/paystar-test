<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Order\Entities\Order;

class PaymentTransaction extends Model
{

    protected $fillable = [
        'token',
        'ref_num',
        'order_id',
        'payment_amount',
        'transaction_id',
        'card_number',
        'transaction_code',
    ];


    // Define the relationship with Order model
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
