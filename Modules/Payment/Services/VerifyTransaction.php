<?php

namespace Modules\Payment\Services;

use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use Modules\Payment\Repositories\PaymentRepository;

class VerifyTransaction
{
    protected $createSignService;
    protected $paymentRepository;

    public function __construct(CreateSignService $createSignService, PaymentRepository $paymentRepository)
    {
        $this->createSignService = $createSignService;
        $this->paymentRepository = $paymentRepository;
    }

    public function verify(array $request)
    {
        try {
            // Retrieve the payment transaction by ref_num from the database
            $paymentTransaction = $this->paymentRepository->findByRefNum($request['ref_num']);


            // Compare the last four digits of the card numbers
            if (!$this->compareCardNumbers($paymentTransaction->order->bank_number, $request['card_number'])) {
                   dd('bank number missmatch');
            }

            // Prepare verification data
            $data = $this->prepareVerificationData($request);

            // Make the HTTP request to verify the transaction
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . '0yovdk2l6e143'
            ])->timeout(10)->post(env("PAYMENT_GATEWAY_VERIFY"), $data);

            if (!$response->successful()) {
                throw new \Exception('Failed to make the HTTP request.');
            }

            {
                $updatedTransaction = $this->paymentRepository->updateTransactionFields($paymentTransaction->id, $request);

                return $updatedTransaction;
                $paymentTransaction->order->markAsPaid();
            }
        } catch (\Exception $e) {
            // Handle exceptions
            if ($e instanceof ValidationException) {
                return back()->withErrors($e->errors())->withInput();
            }
            return back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    private function compareCardNumbers($maskedCardNumber, $fullCardNumber)
    {
        // Extract the last four digits from the masked card number
        $maskedDigits = substr($maskedCardNumber, -4);
        // Extract the last four digits from the full card number
        $fullDigits = substr($fullCardNumber, -4);

        // Compare the last four digits
        return ($maskedDigits === $fullDigits);
    }

    public function prepareVerificationData(array $data)
    {
        $paymentTransaction = $this->paymentRepository->findByRefNum($data['ref_num']);
        $amount = $paymentTransaction->order->total_price;
        $refNum = $data['ref_num'];
        $cardNumber = $data['card_number'];
        $trackingCode = $data['tracking_code'];

        $signature = $this->createSignService->createSignature([$amount, $refNum, $cardNumber, $trackingCode], env("PAYMENT_GATEWAY_TOKEN"));

        return [
            'ref_num' => $refNum,
            'amount' => $amount,
            'sign' => $signature,
        ];
    }
}
