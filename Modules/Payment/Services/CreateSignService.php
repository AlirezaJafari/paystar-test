<?php

namespace Modules\Payment\Services;

class CreateSignService
{

    public function createSignature($data, $key)
    {
        // Prepare the data for the signature
        $signatureData = implode('#', $data);

        // Generate the signature using HMAC with SHA512 encryption algorithm
        $signature = hash_hmac('sha512', $signatureData, $key);

        return $signature;
    }



}
