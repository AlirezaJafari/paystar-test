<?php

namespace Modules\Payment\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Modules\Payment\Entities\PaymentTransaction;
use Modules\Payment\Repositories\PaymentRepository;
use Modules\Payment\Services\CreateSignService;

class CreateTransaction
{
    protected $paymentRepository;
    protected $createSignService;
    protected $gatewayId;
    protected $code;

    public function __construct(PaymentRepository $paymentRepository, CreateSignService $createSignService)
    {
        $this->gatewayId = env("PAYMENT_GATEWAY_ID");
        $this->code = env("PAYMENT_GATEWAY_TOKEN");
        $this->paymentRepository = $paymentRepository;
        $this->createSignService = $createSignService;
    }

    public function createPaymentTransaction(array $data)
    {
        try {
            $requestData = [
                'amount' => $data['total_price'],
                'order_id' => $data['order_id'],
                'callback' => env("PAYMENT_GATEWAY_CALLBACK"),
            ];

            $signature = $this->createSignService->createSignature($requestData, $this->code);

            $requestData['signature'] = $signature;

            // Make the payment request
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->gatewayId,
            ])->post(env("PAYMENT_GATEWAY_CREATE"), $requestData);


            // Check if the request was successful
            if ($response->successful()) {
                // Save the payment transaction data to the database
                $paymentTransaction = $this->paymentRepository->createPaymentTransaction($response['data']);
               return $paymentTransaction;
            } else {
                // Handle the error response
                return ['success' => false, 'error' => 'Failed to create payment transaction.'];
            }
        } catch (\Illuminate\Http\Client\RequestException $e) {
            // Handle request exceptions (e.g., 404, 500)
            return ['success' => false, 'error' => 'Request error: ' . $e->getMessage()];
        } catch (\Exception $e) {
            // Handle other exceptions
            return ['success' => false, 'error' => 'An error occurred: ' . $e->getMessage()];
        }
    }

}
