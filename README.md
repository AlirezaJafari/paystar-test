# Paystar App

a App is a simple web application built with Laravel that allows users to manage their tasks.

## Features

Introduction
The Paystar Test project is built using Laravel, a powerful PHP framework. It provides functionalities to manage orders, including creating, viewing, and updating orders.

Features
Order Creation: Users can create new orders with associated items.
Order Viewing: Users can view details of existing orders.
Online Payment: The system supports online payment for orders using payment gateways integrated via Guzzle.

## Packages Used

The following packages are used in this project:

- **Guzzle**: Used for making HTTP requests.
- **Laravel Sanctum**: Provides a simple API authentication system.
- **Laravel Modules**: Allows modularization of the Laravel application.
- **L5 Repository**: Provides a simplified and consistent repository pattern for Laravel.

To add these packages to your project, run `composer require` followed by the package name.


## Installation

1. Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/AlirezaJafari/paystar-test.git

    ```

2. Navigate to the project directory:

    ```bash
    cd paystar-test
    ```

3. Install PHP dependencies using Composer:

    ```bash
    composer install
    ```

5. Copy the .env.example file to .env and configure your environment variables:

    ```bash
    cp .env.example .env
    ```

6. Generate an application key:

    ```bash
    php artisan key:generate
    ```

7. Run database migrations to create the necessary tables:

    ```bash
    php artisan migrate
    ```

8. (Optional) Seed the database with sample data:

    ```bash
    cd paystar-test
    ```

9. Start the development server:

    ```bash
    php artisan serve
    ```

10. Visit [http://localhost:8000](http://localhost:8000) in your web browser to access the application.

## Usage
Register for an account or log in if you already have one.
Create new orders by navigating to the create order page.
View existing orders on the dashboard.
Pay for orders online using the integrated payment gateway.
add you token ad gateway id to env

## Testing

To run the PHPUnit tests:

```bash
php artisan test
