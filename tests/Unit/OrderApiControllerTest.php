<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Controllers\Api\OrderController;
use Modules\Order\Http\Resources\OrderResource;
use Modules\Order\Repositories\OrderRepository;
use Modules\User\Entities\User;
use Tests\TestCase;

/** @test */

class OrderApiControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_stores_order_via_api()
    {
        // Mock the OrderRepository
        $orderRepository = $this->mock(OrderRepository::class);

        // Mock the Request and set the request data
        $requestData = ['some' => 'data']; // Replace with your request data
        $request = new Request($requestData);

        // Mock the authenticated user
        $user = factory(User::class)->create();
        Auth::shouldReceive('user')->andReturn($user);

        // Mock the created order
        $order = factory(Order::class)->create();
        $orderRepository->shouldReceive('createOrderWithItems')->with($requestData, $user->id)->andReturn($order);

        // Create an instance of the OrderController with the mocked OrderRepository
        $controller = new OrderController($orderRepository);

        // Call the store method
        $response = $controller->store($request);

        // Assert that the response is an instance of OrderResource
        $this->assertInstanceOf(OrderResource::class, $response);

        // You can assert further against the structure or content of the returned resource if needed
    }

    // Helper function to mock a class
    protected function mock($class)
    {
        $mock = \Mockery::mock($class);
        $this->app->instance($class, $mock);

        return $mock;
    }
}

