<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paystar Transaction</title>
</head>
<body onload="submitForm()">
<form id="paystarForm" action="https://core.paystar.ir/api/pardakht/payment" method="POST" style="display: none;">
    <!-- Hidden inputs -->
    <input type="hidden" name="token" value={{$paymentTransaction->token}}> <!-- Example signature (you need to calculate this) -->
    <!-- Add any other optional parameters as hidden inputs here -->
</form>

<!-- JavaScript to submit the form automatically -->
<script>
    function submitForm() {
        document.getElementById('paystarForm').submit();
    }
</script>
</body>
</html>

