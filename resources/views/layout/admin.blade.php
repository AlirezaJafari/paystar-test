<!DOCTYPE html>
<html>
<head>
    @include('layout.css')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @include('layout.navbar')
    @include('layout.sidebar')
    @yield('body')
    <div class="content-wrapper" style="min-height: 639px;">

    </div>
{{--    @include('admin.layout.footer')--}}
</div>
@include('layout.js')
</body>
</html>
